<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Album;
use App\Gallery;

class HomeController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('frontend.home');
    }

    public function about()
    {
        return view('frontend.about');
    }

    public function facilities()
    {
        return view('frontend.facilities');
    }

    public function blog()
    {
        return view('frontend.blogs');
    }

    public function contact()
    {
        return view('frontend.contact');
    }
    public function albums()
    {
        // dd("Asd");
        $albums=Album::orderBy('created_at','desc')->paginate(6);
        return view('frontend.albums')->with('albums',$albums);
        
    }
    public function albumImages($slug)
    {
        $album=Album::where('slug',$slug)->firstOrFail();
        $images=$album->album_images()->paginate(9);
       
        return view('frontend.album_images')->with('album',$album)->with('images',$images);
    }
}
