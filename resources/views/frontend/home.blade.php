@extends('layouts.app')

@section('body')
    <!-- Revolution Slider -->
    <div class="g-overflow-hidden">
        <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="typewriter-effect" data-source="gallery" style="background-color:transparent;padding:0px;">
          <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
          <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
            <ul>  <!-- SLIDE  -->
              <li data-index="rs-2800" data-transition="curtain-1" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="dental1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/dental1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper "
                     id="slide-2800-layer-7"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                     data-width="full"
                     data-height="full"
                     data-whitespace="nowrap"

                     data-type="shape"
                     data-basealign="slide"
                     data-responsive_offset="off"
                     data-responsive="off"
                     data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 5;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                     id="slide-2800-layer-1"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']"
                     data-fontsize="['37','34','25','30']"
                     data-lineheight="['37','34','25','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['center','center','center','center']"
                     data-paddingtop="[20,20,20,20]"
                     data-paddingright="[40,40,40,40]"
                     data-paddingbottom="[20,20,20,20]"
                     data-paddingleft="[40,40,40,40]"

                     style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);border-top: 2px solid #fff; border-bottom: 2px solid #fff;">
                     <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i>
                     CARE DENTAL HOME
                     <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 ml-2 icon-media-119 u-line-icon-pro"></i>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                     id="slide-2800-layer-2"
                     data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
                     data-fontsize="['27','27','25','30']"
                     data-lineheight="['40','40','35','40']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['center','center','center','center']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 8; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);border-width:0px;">
                     Creative freedom matters user experience.<br>We minimize the gap between technology and its audience.
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-4"
                     id="slide-2800-layer-3"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','150']"
                     data-width="100"
                     data-height="1"
                     data-whitespace="nowrap"

                     data-type="shape"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                     id="slide-2800-layer-4"
                     data-x="['right','right','center','center']" data-hoffset="['630','525','-105','100']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                     data-responsive_offset="on"

                     data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[50,50,50,50]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[50,50,50,50]"

                     style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More
                </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                     href="#portfolio-section" target="_self" id="slide-2800-layer-5"
                     data-x="['left','left','center','center']" data-hoffset="['630','525','105','100']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                     data-responsive_offset="on"

                     data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[50,50,50,50]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[50,50,50,50]"

                     style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Our Work
                </div>
              </li>
              <li data-index="rs-2801" data-transition="slideup" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/dental2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/dental2.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper "
                     id="slide-2801-layer-7"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                     data-width="full"
                     data-height="full"
                     data-whitespace="nowrap"

                     data-type="shape"
                     data-basealign="slide"
                     data-responsive_offset="off"
                     data-responsive="off"
                     data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 5;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                     id="slide-2801-layer-1"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']"
                     data-fontsize="['37','34','25','30']"
                     data-lineheight="['37','34','25','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['center','center','center','center']"
                     data-paddingtop="[20,20,20,20]"
                     data-paddingright="[40,40,40,40]"
                     data-paddingbottom="[20,20,20,20]"
                     data-paddingleft="[40,40,40,40]"

                     style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);border-top: 2px solid #fff; border-bottom: 2px solid #fff;">
                     <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i>
                     DEDICATED ADVANCED TEAM
                     <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 ml-2 icon-media-119 u-line-icon-pro"></i>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                     id="slide-2801-layer-2"
                     data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
                     data-fontsize="['27','27','25','30']"
                     data-lineheight="['40','40','35','40']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['center','center','center','center']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 8; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);border-width:0px;">
                    Care Dental Home<br>key digital services on web and mobile.
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-4"
                     id="slide-2801-layer-3"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','150']"
                     data-width="100"
                     data-height="1"
                     data-whitespace="nowrap"

                     data-type="shape"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                     id="slide-2801-layer-4"
                     data-x="['right','right','center','center']" data-hoffset="['630','525','-105','100']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                     data-responsive_offset="on"

                     data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[50,50,50,50]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[50,50,50,50]"

                     style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More
                </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                     href="#portfolio-section" target="_self" id="slide-2801-layer-5"
                     data-x="['left','left','center','center']" data-hoffset="['630','525','105','100']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                     data-responsive_offset="on"

                     data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[50,50,50,50]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[50,50,50,50]"

                     style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Our Work
                </div>
              </li>
              <li data-index="rs-2802" data-transition="slidedown" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="/dental3.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/dental3.jpg" alt="" data-bgposition="center center" data-bgfit="cover" class="rev-slidebg">
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-shape tp-shapewrapper "
                     id="slide-2802-layer-7"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                     data-width="full"
                     data-height="full"
                     data-whitespace="nowrap"

                     data-type="shape"
                     data-basealign="slide"
                     data-responsive_offset="off"
                     data-responsive="off"
                     data-frames='[{"from":"opacity:0;","speed":500,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},{"speed":5000,"to":"opacity:0;","ease":"Power4.easeInOut"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 5;background-color:rgba(0, 0, 0, 0.3);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                     id="slide-2802-layer-1"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['-100','-100','-100','-100']"
                     data-fontsize="['37','34','25','30']"
                     data-lineheight="['37','34','25','30']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['center','center','center','center']"
                     data-paddingtop="[20,20,20,20]"
                     data-paddingright="[40,40,40,40]"
                     data-paddingbottom="[20,20,20,20]"
                     data-paddingleft="[40,40,40,40]"

                     style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);border-top: 2px solid #fff; border-bottom: 2px solid #fff;">
                     <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 mr-2 icon-media-119 u-line-icon-pro"></i>
                     WE DO THINGS DIFFERENTLY
                     <i class="g-hidden-sm-down g-font-size-20 g-pos-rel g-top-minus-5 ml-2 icon-media-119 u-line-icon-pro"></i>
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption   tp-resizeme rs-parallaxlevel-4"
                     id="slide-2802-layer-2"
                     data-x="['center','center','center','center']" data-hoffset="['-10','-10','-10','-10']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['50','50','50','50']"
                     data-fontsize="['27','27','25','30']"
                     data-lineheight="['40','40','35','40']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="text"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['center','center','center','center']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 8; white-space: nowrap; font-weight: 400; color: rgba(255, 255, 255, 1.00);border-width:0px;">
                     Creative freedom matters user experience.<br>We minimize the gap between technology and its audience.
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-4"
                     id="slide-2802-layer-3"
                     data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','150']"
                     data-width="100"
                     data-height="1"
                     data-whitespace="nowrap"

                     data-type="shape"
                     data-responsive_offset="on"

                     data-frames='[{"from":"y:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[0,0,0,0]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[0,0,0,0]"

                     style="z-index: 7;background-color:rgba(255, 255, 255, 1.00);border-color:rgba(0, 0, 0, 0);border-width:0px;"></div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                     id="slide-2802-layer-4"
                     data-x="['right','right','center','center']" data-hoffset="['630','525','-105','100']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                     data-responsive_offset="on"

                     data-frames='[{"from":"x:-50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[50,50,50,50]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[50,50,50,50]"

                     style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Learn More
                </div>

                <!-- LAYER NR. 7 -->
                <div class="tp-caption rev-btn  tp-resizeme rs-parallaxlevel-4"
                     href="#portfolio-section" target="_self" id="slide-2802-layer-5"
                     data-x="['left','left','center','center']" data-hoffset="['630','525','105','100']"
                     data-y="['middle','middle','middle','middle']" data-voffset="['240','240','240','240']"
                     data-width="none"
                     data-height="none"
                     data-whitespace="nowrap"

                     data-type="button"
                     data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                     data-responsive_offset="on"

                     data-frames='[{"from":"x:50px;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"150","ease":"Power2.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 0);bw:2px 2px 2px 2px;"}]'
                     data-textAlign="['left','left','left','left']"
                     data-paddingtop="[0,0,0,0]"
                     data-paddingright="[50,50,50,50]"
                     data-paddingbottom="[0,0,0,0]"
                     data-paddingleft="[50,50,50,50]"

                     style="z-index: 9; white-space: nowrap; font-size: 15px; line-height: 46px; color: rgba(255, 255, 255, 1.00);background-color:rgba(0, 0, 0, 0);border-color:rgba(255, 255, 255, 1);border-style:solid;border-width:2px;border-radius:30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Our Work
                </div>
              </li>
            </ul>
            <div class="tp-bannertimer" style="height: 2px; background-color: rgba(255, 255, 255, .5);"></div> </div>
          </div>
        </div>
      </div>
      <!-- End Revolution Slider -->

      <!-- About -->
      <div id="about-section" class="g-overflow-hidden">
        <div class="container g-pt-100 g-pb-50">
          <!-- Heading -->
          <div class="g-max-width-550 text-center mx-auto g-mb-100">
            <h1 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-18 g-letter-spacing-2 mb-4">Our Facilities</h1>
            <p class="g-font-size-16">Unify creative technology company providing key digital services. Focused on helping our clients to build a successful business on web and mobile.</p>
          </div>
          <!-- End Heading -->

        <div class="row">
                <div class="col-lg-4 g-mb-30">
                  <!-- Article -->
                  <article class="u-block-hover g-brd-around g-brd-gray-light-v4">
                    <figure class="g-overflow-hidden">
                      <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/fac1.jpg" alt="Image Description">
                    </figure>
              
                    <div class="g-pa-30-30-20">
                      <h3 class="h6 text-uppercase g-font-weight-600 g-mb-10">
                        <a class="g-color-gray-dark-v2 g-text-underline--none--hover" href="#">Mauris tellus magna, pretium</a>
                      </h3>
              
                      <p>Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem vel.</p>
                    </div>
                  </article>
                  <!-- End Article -->
                </div>
              
                <div class="col-lg-4 g-mb-30">
                  <!-- Article -->
                  <article class="u-block-hover g-brd-around g-brd-gray-light-v4">
                    <figure class="g-overflow-hidden">
                      <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/fac2.jpg" alt="Image Description">
                    </figure>
              
                    <div class="g-pa-30-30-20">
                      <h3 class="h6 text-uppercase g-font-weight-600 g-mb-10">
                        <a class="g-color-gray-dark-v2 g-text-underline--none--hover" href="#">Aenean malesuada a semased facili</a>
                      </h3>
              
                      <p>Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem vel.</p>
                    </div>
                  </article>
                  <!-- End Article -->
                </div>
              
                <div class="col-lg-4 g-mb-30">
                  <!-- Article -->
                  <article class="u-block-hover g-brd-around g-brd-gray-light-v4">
                    <figure class="g-overflow-hidden">
                      <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/fac3.jpg" alt="Image Description">
                    </figure>
              
                    <div class="g-pa-30-30-20">
                      <h3 class="h6 text-uppercase g-font-weight-600 g-mb-10">
                        <a class="g-color-gray-dark-v2 g-text-underline--none--hover" href="#">Donec lectus fringilla imperdiet</a>
                      </h3>
              
                      <p>Sed feugiat porttitor nunc, non dignissim ipsum vestibulum in. Donec in blandit dolor. Vivamus a fringilla lorem vel.</p>
                    </div>
                  </article>
                  <!-- End Article -->
                </div>
        </div>

            <div class="text-center">
            <a class="btn u-btn-primary g-bg-secondary g-color-primary g-color-white--hover g-bg-primary--hover g-font-weight-600 g-font-size-12 g-rounded-30 g-py-15 g-px-35" href="#!">View All</a>
            </div>
        </div>

      </div>
      <!-- End About -->

      <!-- Quote -->
      <div class="dzsparallaxer auto-init height-is-based-on-content use-loading" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
        <div class="divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_4--after" style="height: 130%; background-image: url(banner1.jpg);"></div>

        <div class="container text-center g-pos-rel g-z-index-1 g-py-100">
          <blockquote class="g-color-white g-font-family-merriweather g-font-size-30 g-font-size-40--md g-line-height-1_4 g-mb-50">
            <span class="g-color-primary g-font-family-ptsans g-font-size-90 g-line-height-0_7 g-pos-rel g-top-30">&#8220;</span>
            Smiling is Fun With Healthy Teeth & Gums
            <span class="g-color-primary g-font-family-ptsans g-font-size-90 g-line-height-0_7 g-pos-rel g-top-30">&#8221;</span>
          </blockquote>
          <h4 class="g-color-white g-font-size-15 text-uppercase mb-1"><span class="d-inline-block align-middle g-width-40 g-height-2 g-bg-primary mr-3"></span> prof. Dr. ninad moon</h4>
        </div>
      </div>
      <!-- End Quote -->

      <!-- Team -->
      <div class="container g-py-100">
        <!-- Heading -->
        <div class="g-max-width-550 text-center mx-auto g-mb-100">
          <h2 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-18 g-letter-spacing-2 mb-4">Meet Our Doctors.</h2>
          <h2 class="h3">Meet our professional dentists.</h2>
        </div>
        <!-- End Heading -->

        <div class="row g-mx-minus-25 g-mb-40">
          <div class="col-6 col-lg-3 g-px-25 g-mb-50">
            <!-- Team -->
            <div class="text-center">
              <img class="u-shadow-v29 g-width-110 g-height-110 rounded-circle mb-5" src="assets/img-temp/200x200/img1.jpg" alt="Image Description">
              <h4 class="h5 g-font-weight-600 mb-1">Alex Taylor</h4>
              <span class="d-block g-color-primary">Founder</span>
              <hr class="g-brd-gray-light-v4 g-my-15">
              <a class="u-link-v5 g-color-text g-color-main--hover g-font-size-13" href="#!">alex@gmail.com</a>
            </div>
            <!-- End Team -->
          </div>

          <div class="col-6 col-lg-3 g-px-25 g-mb-50">
            <!-- Team -->
            <div class="text-center">
              <img class="u-shadow-v29 g-width-110 g-height-110 rounded-circle mb-5" src="assets/img-temp/200x200/img3.jpg" alt="Image Description">
              <h4 class="h5 g-font-weight-600 mb-1">Kate Metu</h4>
              <span class="d-block g-color-primary">Manager</span>
              <hr class="g-brd-gray-light-v4 g-my-15">
              <a class="u-link-v5 g-color-text g-color-main--hover g-font-size-13" href="#!">kate@gmail.com</a>
            </div>
            <!-- End Team -->
          </div>

          <div class="col-6 col-lg-3 g-px-25 g-mb-50">
            <!-- Team -->
            <div class="text-center">
              <img class="u-shadow-v29 g-width-110 g-height-110 rounded-circle mb-5" src="assets/img-temp/200x200/img4.jpg" alt="Image Description">
              <h4 class="h5 g-font-weight-600 mb-1">Daniel Wearne</h4>
              <span class="d-block g-color-primary">Developer</span>
              <hr class="g-brd-gray-light-v4 g-my-15">
              <a class="u-link-v5 g-color-text g-color-main--hover g-font-size-13" href="#!">daniel@gmail.com</a>
            </div>
            <!-- End Team -->
          </div>

          <div class="col-6 col-lg-3 g-px-25 g-mb-50">
            <!-- Team -->
            <div class="text-center">
              <img class="u-shadow-v29 g-width-110 g-height-110 rounded-circle mb-5" src="assets/img-temp/200x200/img2.jpg" alt="Image Description">
              <h4 class="h5 g-font-weight-600 mb-1">Tina Krueger</h4>
              <span class="d-block g-color-primary">Designer</span>
              <hr class="g-brd-gray-light-v4 g-my-15">
              <a class="u-link-v5 g-color-text g-color-main--hover g-font-size-13" href="#!">tina@gmail.com</a>
            </div>
            <!-- End Team -->
          </div>
        </div>

      </div>
      <!-- End Team -->

   

      <!-- Counters -->
      <div class="dzsparallaxer auto-init height-is-based-on-content use-loading" data-options='{direction: "reverse", settings_mode_oneelement_max_offset: "150"}'>
        <div class="divimage dzsparallaxer--target w-100 g-bg-size-cover g-bg-img-hero g-bg-cover g-bg-black-opacity-0_7--after" style="height: 130%; background-image: url(banner-2.jpg);"></div>

        <div class="container g-pt-100 g-pb-70">
          <div class="row">
            <div class="col-sm-6 col-lg-3 align-items-end mt-auto g-mb-50">
              <div class="text-center">
                <span class="js-counter g-color-primary g-font-weight-600 g-font-size-60 g-line-height-0_9 mr-2">75</span>
                <h3 class="d-inline-block g-color-white g-font-weight-600 g-font-size-22 mb-0">Patients</h3>
              </div>
            </div>

            <div class="col-sm-6 col-lg-3 align-items-end mt-auto g-mb-50">
              <div class="text-center">
                <span class="js-counter g-color-primary g-font-weight-600 g-font-size-60 g-line-height-0_9 mr-2">20</span>
                <h3 class="d-inline-block g-color-white g-font-weight-600 g-font-size-22 mb-0">Staffs</h3>
              </div>
            </div>

            <div class="col-sm-6 col-lg-3 align-items-end mt-auto g-mb-50">
              <div class="text-center">
                <span class="js-counter g-color-primary g-font-weight-600 g-font-size-60 g-line-height-0_9 mr-2">15</span>
                <h3 class="d-inline-block g-color-white g-font-weight-600 g-font-size-22 mb-0">Members</h3>
              </div>
            </div>

            <div class="col-sm-6 col-lg-3 align-items-end mt-auto g-mb-50">
              <div class="text-center">
                <span class="js-counter g-color-primary g-font-weight-600 g-font-size-60 g-line-height-0_9 mr-2">50</span>
                <h3 class="d-inline-block g-color-white g-font-weight-600 g-font-size-22 mb-0">Projects</h3>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Counters -->

    
      <!-- News -->
      <div id="news-section" class="g-bg-secondary g-py-100">
        <div class="container">
          <!-- Heading -->
          <div class="g-max-width-550 text-center mx-auto g-mb-70">
            <h2 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-13 g-letter-spacing-2 mb-4">News Blog</h2>
            <h2 class="h3 mb-5">Read the latest news and blogs.</h2>
          </div>
          <!-- End Heading -->

          <div class="row g-mx-minus-25 g-mb-50">
            <div class="col-lg-6 g-px-25 g-mb-50">
              <!-- Blog Grid Modern Blocks -->
              <article class="row align-items-stretch no-gutters u-shadow-v29 g-transition-0_3">
                <div class="col-sm-6 g-bg-white g-rounded-left-5">
                  <div class="g-pa-35">
                    <ul class="list-inline g-color-gray-dark-v4 g-font-weight-600 g-font-size-12">
                      <li class="list-inline-item mr-0">Alex Teseira</li>
                      <li class="list-inline-item mx-2">&#183;</li>
                      <li class="list-inline-item">5 June 2017</li>
                    </ul>

                    <h2 class="h5 g-color-black g-font-weight-600 mb-4">
                      <a class="u-link-v5 g-color-black g-color-primary--hover g-cursor-pointer" href="#!">Announcing a free plan for small teams</a>
                    </h2>
                    <p class="g-color-gray-dark-v4 g-line-height-1_8 mb-4">At Wake, our mission has always been focused on bringing.</p>

                    <ul class="list-inline g-font-size-12 mb-0">
                      <li class="list-inline-item g-mb-10">
                        <a class="u-tags-v1 g-color-blue g-bg-blue-opacity-0_1 g-bg-blue--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15" href="#!">Start-Up</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-6 g-bg-size-cover g-bg-pos-center g-min-height-300 g-rounded-right-5" data-bg-img-src="assets/img-temp/400x500/img1.jpg"></div>
              </article>
              <!-- End Blog Grid Modern Blocks -->
            </div>

            <div class="col-lg-6 g-px-25 g-mb-50">
              <!-- Blog Grid Modern Blocks -->
              <article class="row align-items-stretch no-gutters u-shadow-v29 g-transition-0_3">
                <div class="col-sm-6 g-bg-white g-rounded-left-5">
                  <div class="g-pa-35">
                    <ul class="list-inline g-color-gray-dark-v4 g-font-weight-600 g-font-size-12">
                      <li class="list-inline-item mr-0">Alex Teseira</li>
                      <li class="list-inline-item mx-2">&#183;</li>
                      <li class="list-inline-item">5 June 2017</li>
                    </ul>

                    <h2 class="h5 g-color-black g-font-weight-600 mb-4">
                      <a class="u-link-v5 g-color-black g-color-primary--hover g-cursor-pointer" href="#!">Exclusive interview with InVision's CEO</a>
                    </h2>
                    <p class="g-color-gray-dark-v4 g-line-height-1_8 mb-4">Clara Valberg is the founder and CEO of InVision.</p>

                    <ul class="list-inline g-font-size-12 mb-0">
                      <li class="list-inline-item g-mb-10">
                        <a class="u-tags-v1 g-color-brown g-bg-brown-opacity-0_1 g-bg-brown--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15" href="#!">Start-Up</a>
                      </li>
                      <li class="list-inline-item g-mb-10">
                        <a class="u-tags-v1 g-color-pink g-bg-pink-opacity-0_1 g-bg-pink--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15" href="#!">Interview</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-6 g-bg-size-cover g-bg-pos-center g-min-height-300 g-rounded-right-5" data-bg-img-src="assets/img-temp/400x500/img2.jpg"></div>
              </article>
              <!-- End Blog Grid Modern Blocks -->
            </div>

            <div class="w-100"></div>

            <div class="col-lg-6 g-px-25 g-mb-50">
              <!-- Blog Grid Modern Blocks -->
              <article class="row align-items-stretch no-gutters u-shadow-v29 g-transition-0_3">
                <div class="col-sm-6 g-bg-white g-rounded-left-5">
                  <div class="g-pa-35">
                    <ul class="list-inline g-color-gray-dark-v4 g-font-weight-600 g-font-size-12">
                      <li class="list-inline-item mr-0">Alex Teseira</li>
                      <li class="list-inline-item mx-2">&#183;</li>
                      <li class="list-inline-item">5 June 2017</li>
                    </ul>

                    <h2 class="h5 g-color-black g-font-weight-600 mb-4">
                      <a class="u-link-v5 g-color-black g-color-primary--hover g-cursor-pointer" href="#!">V’s Barbershop Opening First South Florida Location</a>
                    </h2>
                    <p class="g-color-gray-dark-v4 g-line-height-1_8 mb-4">There’s nothing like a good barbershop.</p>

                    <ul class="list-inline g-font-size-12 mb-0">
                      <li class="list-inline-item g-mb-10">
                        <a class="u-tags-v1 g-color-bluegray g-bg-bluegray-opacity-0_1 g-bg-bluegray--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15" href="#!">Barber</a>
                      </li>
                      <li class="list-inline-item g-mb-10">
                        <a class="u-tags-v1 g-color-primary g-bg-primary-opacity-0_1 g-bg-primary--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15" href="#!">Style</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-6 g-bg-size-cover g-bg-pos-center g-min-height-300 g-rounded-right-5" data-bg-img-src="assets/img-temp/400x500/img3.jpg"></div>
              </article>
              <!-- End Blog Grid Modern Blocks -->
            </div>

            <div class="col-lg-6 g-px-25 g-mb-50">
              <!-- Blog Grid Modern Blocks -->
              <article class="row align-items-stretch no-gutters u-shadow-v29 g-transition-0_3">
                <div class="col-sm-6 g-bg-white g-rounded-left-5">
                  <div class="g-pa-35">
                    <ul class="list-inline g-color-gray-dark-v4 g-font-weight-600 g-font-size-12">
                      <li class="list-inline-item mr-0">Alex Teseira</li>
                      <li class="list-inline-item mx-2">&#183;</li>
                      <li class="list-inline-item">5 June 2017</li>
                    </ul>

                    <h2 class="h5 g-color-black g-font-weight-600 mb-4">
                      <a class="u-link-v5 g-color-black g-color-primary--hover g-cursor-pointer" href="#!">The Hypnotic Allure of Cinemagraphic Waves</a>
                    </h2>
                    <p class="g-color-gray-dark-v4 g-line-height-1_8 mb-4">Ocean waves are, by definition, in a constant state of motion.</p>

                    <ul class="list-inline g-font-size-12 mb-0">
                      <li class="list-inline-item g-mb-10">
                        <a class="u-tags-v1 g-color-brown g-bg-brown-opacity-0_1 g-bg-brown--hover g-color-white--hover g-rounded-50 g-py-4 g-px-15" href="#!">Animation</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-6 g-bg-size-cover g-bg-pos-center g-min-height-300 g-rounded-right-5" data-bg-img-src="assets/img-temp/400x500/img4.jpg"></div>
              </article>
              <!-- End Blog Grid Modern Blocks -->
            </div>
          </div>

          <div class="text-center">
            <a class="btn u-btn-outline-primary g-font-weight-600 g-font-size-12 g-rounded-30 g-py-15 g-px-35" href="">Read more News</a>
          </div>
        </div>
      </div>
      <!-- End News -->

      <!-- Testimonials -->
      <div class="container g-pt-70 g-pb-100">
           <div class="g-max-width-550 text-center mx-auto g-mb-70">
            <h2 class="text-uppercase g-color-main-light-v1 g-font-weight-600 g-font-size-18 g-letter-spacing-2 mb-4">Our Feedbacks</h2>
            <h2 class="h3 mb-5">Reviews from our patients</h2>
          </div>
        <div id="carouselCus2" class="js-carousel u-carousel-indicators-v32 g-pos-stc g-width-200 text-center mx-auto g-mb-20"
         data-infinite="true"
         data-center-mode="true"
         data-slides-show="3"
         data-is-thumbs="true"
         data-nav-for="#carouselCus1">
          <div class="js-slide d-flex text-center g-opacity-1 g-cursor-pointer g-px-13 g-py-50">
            <img class="u-shadow-v22 u-carousel-indicators-v32-img g-width-40 g-height-40 g-brd-around g-brd-2 g-brd-transparent rounded-circle" src="assets/img-temp/100x100/img3.jpg" alt="Image Description">
          </div>

          <div class="js-slide d-flex text-center g-opacity-1 g-cursor-pointer g-px-13 g-py-50">
            <img class="u-shadow-v22 u-carousel-indicators-v32-img g-width-40 g-height-40 g-brd-around g-brd-2 g-brd-transparent rounded-circle" src="assets/img-temp/100x100/img2.jpg" alt="Image Description">
          </div>

          <div class="js-slide d-flex text-center g-opacity-1 g-cursor-pointer g-px-13 g-py-50">
            <img class="u-shadow-v22 u-carousel-indicators-v32-img g-width-40 g-height-40 g-brd-around g-brd-2 g-brd-transparent rounded-circle" src="assets/img-temp/100x100/img1.jpg" alt="Image Description">
          </div>

          <div class="js-slide d-flex text-center g-opacity-1 g-cursor-pointer g-px-13 g-py-50">
            <img class="u-shadow-v22 u-carousel-indicators-v32-img g-width-40 g-height-40 g-brd-around g-brd-2 g-brd-transparent rounded-circle" src="assets/img-temp/100x100/img4.jpg" alt="Image Description">
          </div>
        </div>

        <div id="carouselCus1" class="js-carousel g-px-50 g-px-200--lg"
             data-infinite="true"
             data-arrows-classes="u-arrow-v1 g-absolute-centered--y g-width-45 g-height-45 g-font-size-30 g-color-main-light-v1 g-color-primary--hover"
             data-arrow-left-classes="fa fa-angle-left g-left-0 g-left-40--lg g-mt-minus-50"
             data-arrow-right-classes="fa fa-angle-right g-right-0 g-right-40--lg g-mt-minus-50"
             data-nav-for="#carouselCus2">

          <div class="js-slide text-center">
            <blockquote class="g-color-text g-font-size-default g-font-size-18--md g-mb-60">Your customer support is the best I have experienced with any theme I have purchased. You have a theme that far exceeds all others. Thanks for providing such a fantastic theme, all your efforts are greatly appreciated on our end.</blockquote>
              <h4 class="g-font-weight-700 g-font-size-15 g-mb-1">Dick Pottorf</h4>
              <span class="d-block g-color-text g-font-size-13">Reason: Unify Support</span>
          </div>

          <div class="js-slide text-center">
            <blockquote class="g-color-text g-font-size-default g-font-size-18--md g-mb-60">Hi there purchased a couple of days ago and the site looks great, big thanks to the htmlstream guys, they gave me some great help with some fiddly setup issues.</blockquote>
              <h4 class="g-font-weight-700 g-font-size-15 g-mb-1">Bastien Rojanawisut</h4>
              <span class="d-block g-color-text g-font-size-13">Reason: Template Quality</span>
          </div>

          <div class="js-slide text-center">
            <blockquote class="g-color-text g-font-size-default g-font-size-18--md g-mb-60">Dear Htmlstream team, I just bought the Unify template some weeks ago. The template is really nice and offers quite a large set of options.</blockquote>
            <h4 class="g-font-weight-700 g-font-size-15 g-mb-1">Alex Pottorf</h4>
              <span class="d-block g-color-text g-font-size-13">Reason: Template Quality</span>
          </div>

          <div class="js-slide text-center">
            <blockquote class="g-color-text g-font-size-default g-font-size-18--md g-mb-60">New Unify template looks great!. Love the multiple layout examples for Shortcodes and the new Show code Copy code snippet feature is brilliant.</blockquote>
              <h4 class="g-font-weight-700 g-font-size-15 g-mb-1">Mark Mcmanus</h4>
              <span class="d-block g-color-text g-font-size-13">Reason: Code Quality</span>
          </div>
        </div>
        <!-- End Carousel -->
      </div>
      <!-- End Testimonials -->

      <section class="container">


        <div class="g-py-100">
            <div class="row justify-content-center">
            <div class="col-lg-9">
                <h3 class="g-color-black g-font-weight-600 text-center mb-1">Make an Appoinment?</h3>
                <h4 class="g-color-black g-font-weight-400 text-center mb-5">Or contact us for any queries</h6>
                <form>
                <div class="row">
                    <div class="col-md-6 form-group g-mb-20">
                    <label class="g-color-gray-dark-v2 g-font-size-13">Name</label>
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15" type="text" placeholder="John Doe">
                    </div>

                    <div class="col-md-6 form-group g-mb-20">
                    <label class="g-color-gray-dark-v2 g-font-size-13">Email</label>
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15" type="email" placeholder="johndoe@gmail.com">
                    </div>

                    <div class="col-md-6 form-group g-mb-20">
                    <label class="g-color-gray-dark-v2 g-font-size-13">Subject</label>
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15" type="text" placeholder="Feedback">
                    </div>

                    <div class="col-md-6 form-group g-mb-20">
                    <label class="g-color-gray-dark-v2 g-font-size-13">Phone</label>
                    <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded-3 g-py-13 g-px-15" type="tel" placeholder="+ (01) 222 33 44">
                    </div>

                    <div class="col-md-12 form-group g-mb-40">
                    <label class="g-color-gray-dark-v2 g-font-size-13">Message</label>
                    <textarea class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover g-resize-none rounded-3 g-py-13 g-px-15" rows="7" placeholder="Hi there, I would like to ..."></textarea>
                    </div>
                </div>

                <div class="text-center">
                    <button class="btn u-btn-primary g-font-weight-600 g-font-size-13 text-uppercase g-rounded-25 g-py-15 g-px-30" type="submit" role="button">Send Request</button>
                </div>
                </form>
            </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
<script>
    var tpj = jQuery;

var revapi1014;
tpj(document).ready(function () {
  if (tpj("#rev_slider_1014_1").revolution == undefined) {
    revslider_showDoubleJqueryError("#rev_slider_1014_1");
  } else {
    revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
      sliderType: "standard",
      jsFileLocation: "revolution/js/",
      sliderLayout: "fullscreen",
      dottedOverlay: "none",
      delay: 9000,
      navigation: {
        keyboardNavigation: "off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation: "off",
        mouseScrollReverse: "default",
        onHoverStop: "off",
        touch: {
          touchenabled: "on",
          swipe_threshold: 75,
          swipe_min_touches: 1,
          swipe_direction: "horizontal",
          drag_block_vertical: false
        },
        arrows: {
          style: "hermes",
          enable: true,
          hide_onmobile: true,
          hide_under: 768,
          hide_onleave: false,
          tmp: '<div class="tp-arr-allwrapper"><div class="tp-arr-imgholder"></div><div class="tp-arr-titleholder">Pocket Consultancy</div></div>',
          left: {
            h_align: "left",
            v_align: "center",
            h_offset: 0,
            v_offset: 0
          },
          right: {
            h_align: "right",
            v_align: "center",
            h_offset: 0,
            v_offset: 0
          }
        }
      },
      responsiveLevels:[1240,1024,778,778],
      gridwidth:[1240,1024,778,480],
      gridheight:[600,500,400,300],
      lazyType: 'smart',
      scrolleffect: {
        fade: "on",
        grayscale: "on",
        on_slidebg: "on",
        on_parallax_layers: "on",
        direction: "top",
        multiplicator_layers: "1.4",
        tilt: "10",
        disable_on_mobile: "off",
      },
      parallax: {
        type: "scroll",
        origo: "slidercenter",
        speed: 400,
        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55],
      },
      shadow: 0,
      spinner: "off",
      stopLoop: 'off',
      stopAfterLoops: -1,
      stopAtSlide: -1,
      shuffle: "off",
      autoHeight: "off",
      fullScreenAutoWidth: "off",
      fullScreenAlignForce: "off",
      fullScreenOffsetContainer: "",
      fullScreenOffset: "0px",
      hideThumbsOnMobile: "off",
      hideSliderAtLimit: 0,
      hideCaptionAtLimit: 0,
      hideAllCaptionAtLilmit: 0,
      debugMode: false,
      fallbacks: {
        simplifyAll: "off",
        nextSlideOnWindowFocus: "off",
        disableFocusListener: false,
      }
    });
  }

  RsTypewriterAddOn(tpj, revapi1014);
});
    </script>

@endsection