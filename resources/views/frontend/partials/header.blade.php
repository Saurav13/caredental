<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title>Care Dental Home</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/logo_ico.png">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Sans%7CMerriweather%7COpen+Sans%3A300%2C400%2C600%2C700%2C800%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-line-pro/style.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/icon-hs/style.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/cubeportfolio-full/cubeportfolio/css/cubeportfolio.min.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/fancybox/jquery.fancybox.css">

    <!-- CSS Template -->
    <link rel="stylesheet" href="/assets/css/styles.op-consulting.css">

    <!-- Revolution Slider -->
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/settings.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/layers.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution/css/navigation.css">
    <link rel="stylesheet" href="/main-assets/assets/vendor/revolution-slider/revolution-addons/typewriter/css/typewriter.css">

    <link  rel="stylesheet" href="/main-assets/assets/vendor/animate.css">
    <link  rel="stylesheet" href="/main-assets/assets/vendor/custombox/custombox.min.css">
    <!-- CSS Customization -->

    <link rel="stylesheet" href="/main-assets/assets/css/unify-core.css">
    <link rel="stylesheet" href="/main-assets/assets/css/unify-components.css">
    <link rel="stylesheet" href="/main-assets/assets/css/unify-globals.css">
  
    <link rel="stylesheet" href="/main-assets/assets/css/custom.css">
    <style>
      .has-error{
        border: 1px solid #DA4453 !important;
      }
      .help-block{
        color: #721c24;
      }
      [ng\:cloak], [ng-cloak], .ng-cloak {
          display: none !important;
        }

    </style>
  </head>

