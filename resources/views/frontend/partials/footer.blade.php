
  <!-- End Contact -->
  

</main>
</div>
<footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20 footer">
    <div class="container">
      <div class="row">
        <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
          <div class="d-lg-flex">
            <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">&#169; 2019 Care Dental. Powered by <strong><a href="http://incubeweb.com/" target="_blank">Incube</a></strong></small>
            
          </div>
        </div>
  
        <div class="col-md-4 align-self-center">
          <ul class="list-inline text-center text-md-right mb-0">
            <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
              <a href="https://www.facebook.com/nsfunctions/" class="g-color-white-opacity-0_5 g-color-white--hover" target="_blank">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="Instagram">
              <a href="https://www.instagram.com/_get_linked/" class="g-color-white-opacity-0_5 g-color-white--hover" target="_blank">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
            <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
              <a href="https://www.linkedin.com/company/getlinkedsolutions" class="g-color-white-opacity-0_5 g-color-white--hover" target="_blank">
                <i class="fa fa-linkedin"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

<!-- JS Global Compulsory -->
<script src="/main-assets/assets/vendor/jquery/jquery.min.js"></script>
<script src="/main-assets/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
<script src="/main-assets/assets/vendor/popper.min.js"></script>
<script src="/main-assets/assets/vendor/bootstrap/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="/main-assets/assets/vendor/appear.js"></script>
<script src="/main-assets/assets/vendor/slick-carousel/slick/slick.js"></script>
<script src="/main-assets/assets/vendor/cubeportfolio-full/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<script src="/main-assets/assets/vendor/dzsparallaxer/dzsparallaxer.js"></script>
<script src="/main-assets/assets/vendor/dzsparallaxer/dzsscroller/scroller.js"></script>
<script src="/main-assets/assets/vendor/dzsparallaxer/advancedscroller/plugin.js"></script>
<script src="/main-assets/assets/vendor/fancybox/jquery.fancybox.js"></script>

<!-- JS Unify -->
<script src="/main-assets/assets/js/hs.core.js"></script>
<script src="/main-assets/assets/js/components/hs.header.js"></script>
<script src="/main-assets/assets/js/helpers/hs.hamburgers.js"></script>
<script src="/main-assets/assets/js/components/hs.scroll-nav.js"></script>
<script src="/main-assets/assets/js/components/hs.counter.js"></script>
<script src="/main-assets/assets/js/components/hs.carousel.js"></script>
<script src="/main-assets/assets/js/components/hs.cubeportfolio.js"></script>
<script src="/main-assets/assets/js/components/hs.popup.js"></script>
<script src="/main-assets/assets/js/components/hs.go-to.js"></script>

<!-- JS Revolution Slider -->
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

<script src="/main-assets/assets/vendor/revolution-slider/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js"></script>

<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script src="/main-assets/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>

<script  src="/main-assets/assets/vendor/custombox/custombox.min.js"></script>

<!-- JS Unify -->
<script  src="/main-assets/assets/js/components/hs.modal-window.js"></script>
<!-- JS Customization -->
<script src="/main-assets/assets/js/custom.js"></script>
<script src="/underscore.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.5/angular.min.js"></script>
@yield('js')
<!-- JS Plugins Init. -->
<script>
  $(document).on('ready', function () {
    // initialization of carousel
    $.HSCore.components.HSCarousel.init('.js-carousel');

    // initialization of header
    $.HSCore.components.HSHeader.init($('#js-header'));
    $.HSCore.helpers.HSHamburgers.init('.hamburger');

    // initialization of go to section
    $.HSCore.components.HSGoTo.init('.js-go-to');

    // initialization of counters
    var counters = $.HSCore.components.HSCounter.init('[class*="js-counter"]');

    // initialization of popups
    $.HSCore.components.HSPopup.init('.js-fancybox');
  });

  // $(window).on('load', function() {
  //   // initialization of HSScrollNav
  //   $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
  //     duration: 700
  //   });

  //   // initialization of cubeportfolio
  //   $.HSCore.components.HSCubeportfolio.init('.cbp');
  // });

  
</script>
</body>
</html>