<style>
    .profile_picture_mini{
        border-radius: 50%;
        height: 2.5rem;
        width: 2.5rem;
        margin-top: -0.5rem;
    }

    html, body {
      height: 100%;
    }

    #wrap {
      min-height: 100%;
      margin-bottom: -50px;
    }

    #main {
      overflow:auto;
      margin-bottom: -50px;
    }

    .footer {
      position: relative;
      /* margin-top: -150px; 
      height: 150px; */
      /* clear:both; */
      /* padding-top:20px; */
    } 
       
    body:before {
        content: "";
        height: 100%;
        float: left;
        width: 0;
        /* margin-top: -32767px; */
        /* thank you Erik J - negate effect of float*/
      } 

  </style>
  <body id="home-section">
      <main>
        <!-- Header -->
        <header id="js-header" class="u-header u-header--sticky-top u-header--change-appearance u-header--change-logo"
                data-header-fix-moment="0">
          <div class="u-header__section g-bg-black-opacity-0_2 g-transition-0_3 g-py-5 g-py-10--md"
               data-header-fix-moment-exclude="g-bg-black-opacity-0_5 g-py-10--md"
               data-header-fix-moment-classes="u-shadow-v19 g-bg-white g-py-5--md">
            <nav class="navbar navbar-expand-lg g-py-0">
              <div class="container g-pos-rel">
                <!-- Logo -->
                <a href="#!" class="js-go-to navbar-brand u-header__logo"
                   data-type="static">
                  <img class="u-header__logo-img u-header__logo-img--main g-width-90" style="height:4rem" src="/assets/img/logo.png" alt="Image description">
                  <img class="u-header__logo-img g-width-90" src="/assets/img/logo-dark.png"  style="height:4rem" alt="Image description">
                </a>
                <!-- End Logo -->
  
                <!-- Navigation -->
                <div class="collapse navbar-collapse align-items-center flex-sm-row" id="navBar">
                  <ul id="js-scroll-nav" class="navbar-nav text-uppercase g-letter-spacing-1 g-font-size-12 g-pt-20 g-pt-0--lg ml-auto">
                    <li class="nav-item g-mr-15--lg g-mb-7 g-mb-0--lg">
                      <a href="/" class="nav-link p-0">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                      <a href="/about-us" class="nav-link p-0">About Us</a>
                    </li>
                    <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                      <a href="/facilities" class="nav-link p-0">Our Facilities</a>
                    </li>
                    <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                      <a href="/blogs" class="nav-link p-0">Blog</a>
                    </li>
                    <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                      <a href="/albums" class="nav-link p-0">Gallery</a>
                    </li>
                    <li class="nav-item g-mx-15--lg g-mb-7 g-mb-0--lg">
                      <a href="#contact-section" class="nav-link p-0">Contact</a>
                    </li>
                    <li class="nav-item g-ml-15--lg">
                      <a href="../../index.html" class="nav-link p-0">Main</a>
                    </li>
                  </ul>
                </div>
                <!-- End Navigation -->
  
                <!-- Responsive Toggle Button -->
                <button class="navbar-toggler btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-15 g-right-0" type="button"
                        aria-label="Toggle navigation"
                        aria-expanded="false"
                        aria-controls="navBar"
                        data-toggle="collapse"
                        data-target="#navBar">
                  <span class="hamburger hamburger--slider">
                    <span class="hamburger-box">
                      <span class="hamburger-inner"></span>
                    </span>
                  </span>
                </button>
                <!-- End Responsive Toggle Button -->
              </div>
            </nav>
          </div>
        </header>